package de.fussballmanager.fussballmanager.services;

import java.util.List;

import org.springframework.stereotype.Service;

import de.fussballmanager.fussballmanager.dto.VereinDto;
import de.fussballmanager.fussballmanager.entites.Spieler;
import de.fussballmanager.fussballmanager.entites.Verein;
import de.fussballmanager.fussballmanager.repositories.VereinsRepository;

// business logic, mainly using the repository right now as you can see
@Service
public class VereinsService {
	
	private VereinsRepository vereinsRepository;
	
	public VereinsService(VereinsRepository vereinsRepository) {
		this.vereinsRepository = vereinsRepository;
	}

	public List<Verein> getVereine() {
		return vereinsRepository.findAll();
	}

	public void save(VereinDto vereinsDto) {
		Verein neuerVerein = new Verein();
		neuerVerein.setVereinsName(vereinsDto.getVereinsName());
		neuerVerein.setFoundedAt(vereinsDto.getFoundedAt());
		vereinsRepository.save(neuerVerein);
		
	}
	
	public Verein getVereinById(int id) {
		return vereinsRepository.findById(id).get();
	}
	
	public void updateVerein(VereinDto vereinDto, int id) {
		Verein currentVerein = vereinsRepository.getOne(id);
		currentVerein.setFoundedAt(vereinDto.getFoundedAt());
		currentVerein.setVereinsName(vereinDto.getVereinsName());
		vereinsRepository.save(currentVerein);
	}

	public void delete(int id) {
		Verein currentVerein = vereinsRepository.getOne(id);
		
		vereinsRepository.delete(currentVerein);
		
	}
	
	

}
