package de.fussballmanager.fussballmanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.fussballmanager.fussballmanager.entites.Spieler;

public interface SpielerRepository extends JpaRepository<Spieler, Integer> {

}
