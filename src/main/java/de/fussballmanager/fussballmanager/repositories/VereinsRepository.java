package de.fussballmanager.fussballmanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.fussballmanager.fussballmanager.entites.Verein;

public interface VereinsRepository extends JpaRepository<Verein, Integer> {

}
