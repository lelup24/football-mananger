package de.fussballmanager.fussballmanager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import de.fussballmanager.fussballmanager.dto.SpielerDto;
import de.fussballmanager.fussballmanager.dto.VereinDto;
import de.fussballmanager.fussballmanager.entites.Spieler;
import de.fussballmanager.fussballmanager.entites.Verein;
import de.fussballmanager.fussballmanager.services.SpielerService;
import de.fussballmanager.fussballmanager.services.VereinsService;

@Controller
@RequestMapping("/vereine")
public class VereinsController {
	
	private VereinsService vereinsService;
	private SpielerService spielerService;
	
	
	// injecting services
	@Autowired
	public VereinsController(VereinsService vereinsService, SpielerService spielerService) {
		this.vereinsService = vereinsService;
		this.spielerService = spielerService;
	}
	
	
	@GetMapping("/all")
	public String getVereine(Model model) {
		
		// create an entity to build submit-form
		Verein neuerVerein = new Verein();
		VereinDto vereinsDto = new VereinDto();
		
		
		// get data from database -> zip it to a list
		List<Verein> vereinsListe = vereinsService.getVereine();
		
		// add models to model so you can use them in the html
		model.addAttribute("verein", neuerVerein);
		model.addAttribute("vereinDto", vereinsDto);
		model.addAttribute("vereine", vereinsListe);
	
		
		return "vereinsliste";
	}
	
	
	/**
	 * Use ModelAttribute to store the received data
	 * @param verein
	 * @return redirect
	 */
	@PostMapping("/submit")
	public String saveVerein(@Valid @ModelAttribute("vereinDto") VereinDto vereinDto, BindingResult theBindingResult, Model model) {
		
		if (theBindingResult.hasErrors()){
			// get data from database -> zip it to a list
			List<Verein> vereinsListe = vereinsService.getVereine();
			model.addAttribute("vereine", vereinsListe);
			return "vereinsliste";		 
	    }
		
		// using the service -> repository (in the service) to insert data into database
		vereinsService.save(vereinDto);
		
		return "redirect:/vereine/all";
	}
	
	
	/**
	 * Detailview for a specific Verein
	 * @param model
	 * @param id -> PathVariable (see the html) 
	 * @return
	 */
	@GetMapping("/details/{id}")
	public String vereinsDetails(Model model, @PathVariable int id) {
		
		
		// I use the PathVariable to get the Verein I want to to get data from
		Verein currentVerein = vereinsService.getVereinById(id);
		SpielerDto spielerDto = new SpielerDto();
		Spieler neuerSpieler = new Spieler();
		
		model.addAttribute("spieler", neuerSpieler);
		
		model.addAttribute("verein", currentVerein);
		model.addAttribute("spielerDto", spielerDto);
		
		
		return "vereinsdetails";
	}
	
	// redirectAttributes nötig für den redirect
	@PostMapping("/submit/{id}")
	public String saveSpieler(@Valid @ModelAttribute("spieler") Spieler spieler,
			BindingResult theBindingResult, @PathVariable int id,
			Model model) {
		
		if (theBindingResult.hasErrors()){
			Verein currentVerein = vereinsService.getVereinById(id);
			model.addAttribute("verein", currentVerein);
			return "vereinsdetails";
	    }
		
		// Get the Verein you want to store the Spieler in
		Verein currentVerein = vereinsService.getVereinById(id);
		
		
		
		
		spieler.setVerein(currentVerein);
		
		// do it!
		spielerService.save(spieler);
		
		
		return "redirect:/vereine/details/{id}";
	}
	
	@PostMapping("/delete/{id}")
	public String deleteVerein(@PathVariable int id) {
		vereinsService.delete(id);
		
		return "redirect:/vereine/all";
	}
	
	
	@GetMapping("/updateVerein/{id}")
	public String updateSeite(Model model, @PathVariable int id) {
		
		VereinDto vereinDto = new VereinDto();
		Verein verein = vereinsService.getVereinById(id);
		vereinDto.setFoundedAt(verein.getFoundedAt());
		vereinDto.setVereinsName(verein.getVereinsName());
		
		model.addAttribute("verein", verein);
		
		
		model.addAttribute("vereinDto", vereinDto);
		
		return "updateverein";
	}
	
	@GetMapping("/spielerdetails/{id}")
	public String spielerDetails(Model model, @PathVariable int id) {
		Spieler currentSpieler = spielerService.getOne(id);
		SpielerDto spielerDto = new SpielerDto();
		spielerDto.setGehalt((int)currentSpieler.getGehalt());
		spielerDto.setNummer(currentSpieler.getNummer());
		spielerDto.setName(currentSpieler.getName());
		
		model.addAttribute("spieler",  currentSpieler);
		model.addAttribute("spielerDto", spielerDto);
		
		
		return "vereinspieler";
	}
	
	
	@PostMapping("/deleteSpieler/{id}")
	public String deleteSpieler(@PathVariable int id, RedirectAttributes redirectAttributes) {
		Spieler currentSpieler = spielerService.getOne(id);
		
		int vereinsId = currentSpieler.getVerein().getId();
		spielerService.delete(id);
		
		redirectAttributes.addAttribute("vereinsId", vereinsId);
		
		
		return "redirect:/vereine/details/{vereinsId}";
	}
	

	
	@PostMapping("/update/{id}")
	public String updateVerein(@Valid @ModelAttribute("vereinDto") VereinDto vereinDto,
			BindingResult theBindingResult, @PathVariable int id,
			Model model) {

		if (theBindingResult.hasErrors()){
			Verein currentVerein = vereinsService.getVereinById(id);
			model.addAttribute("verein", currentVerein);
			return "updateverein";
	    }
		
		vereinsService.updateVerein(vereinDto, id);
		
		return "redirect:/vereine/details/{id}";
	}
	
	@PostMapping("/updateSpieler/{id}")
	public String updateSpieler(@Valid @ModelAttribute("spielerDto") SpielerDto spielerDto,
			BindingResult theBindingResult, @PathVariable int id,
			Model model) {

		if (theBindingResult.hasErrors()){
			Spieler currentSpieler = spielerService.getOne(id);
			model.addAttribute("spieler", currentSpieler);
			return "vereinspieler";
	    }
		
		spielerService.update(spielerDto, id);
		
		return "redirect:/vereine/spielerdetails/{id}";
	}
	
	

}
