package de.fussballmanager.fussballmanager.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;



public class SpielerDto {
	
	@NotEmpty
	private String name;
	@Min(1850)
	private int gehalt;
	
	@Min(0)
	private int nummer;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGehalt() {
		return gehalt;
	}

	public void setGehalt(int gehalt) {
		this.gehalt = gehalt;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}
	

}
